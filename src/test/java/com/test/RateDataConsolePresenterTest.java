package com.test;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RateDataConsolePresenterTest {

    final String expectedOutput = "" +
            "Requested amount: £10\n" +
            "Rate: 0.8%\n" +
            "Monthly repayment: £0.33\n" +
            "Total repayment: £11.97\n";

    @Test
    public void present() {
        RateData data = new RateData(BigDecimal.TEN, 4, new BigDecimal("0.8"), new BigDecimal("0.33"), new BigDecimal("11.97"));
        RateDataPresenter presenter = new RateDataConsolePresenter(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        presenter.present(new PrintStream(outputStream));
        String output = new String(outputStream.toByteArray(), StandardCharsets.UTF_8);
        assertThat(output, is(equalTo(expectedOutput)));
    }
}