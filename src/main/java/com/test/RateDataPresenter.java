package com.test;

import java.io.PrintStream;

public interface RateDataPresenter {

    void present(PrintStream out);
}
