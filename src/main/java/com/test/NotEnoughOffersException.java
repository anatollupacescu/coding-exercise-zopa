package com.test;

public class NotEnoughOffersException extends RuntimeException {

    public NotEnoughOffersException() {
        super("There are not enough offers available");
    }
}
