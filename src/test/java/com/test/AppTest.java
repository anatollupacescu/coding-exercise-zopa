package com.test;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class AppTest {

    public static final String FILE_PATH = "src/main/resources/data.csv";

    @Test(expected = IllegalArgumentException.class)
    public void notWorkWithValuesBellow1000() {
        App app = new App(FILE_PATH, 36);
        assertThat(app, is(notNullValue()));
        app.processAmount("100");
    }

    @Test(expected = IllegalArgumentException.class)
    public void notWorkWithValuesAbove15k() {
        App app = new App(FILE_PATH, 36);
        assertThat(app, is(notNullValue()));
        app.processAmount("16000");
    }

    @Test(expected = IllegalArgumentException.class)
    public void notWorkWithValuesNotDivisibleBy100() {
        App app = new App(FILE_PATH, 36);
        assertThat(app, is(notNullValue()));
        app.processAmount("13535");
    }

    @Test
    public void acceptValidBorrowingAmount() {
        App app = new App(FILE_PATH, 36);
        assertThat(app, is(notNullValue()));
        app.processAmount("2000");
        //TODO add assertions
    }

    @Test(expected = NotEnoughOffersException.class)
    public void notAcceptAmountsLargerThanOffered() {
        App app = new App(FILE_PATH, 36);
        assertThat(app, is(notNullValue()));
        app.processAmount("9000");
    }
}