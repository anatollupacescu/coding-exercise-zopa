package com.test;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PrioritizedLenderPoolTest {

    private final Set<Offer> offers = new HashSet<>();
    private final OfferPrioritizer doNothingPrioritizer = (list) -> list;

    @Before
    public void setUp() {
        Offer offer1 = new Offer("Bob", "0.075", 640);
        Offer offer2 = new Offer("Jane", "0.069", 48);
        Offer offer3 = new Offer("Fred", "0.071", 520);
        offers.add(offer1);
        offers.add(offer2);
        offers.add(offer3);
    }

    @Test(expected = NotEnoughOffersException.class)
    public void canNotBorrowWhenNoOffers() {
        Set<Offer> offers = Collections.emptySet();
        PrioritizedLenderPool lenderPool = new PrioritizedLenderPool(offers, doNothingPrioritizer);
        lenderPool.borrow(100);
    }

    @Test(expected = NotEnoughOffersException.class)
    public void canNotBorrowMoreThenOffer() {
        Set<Offer> offers = Collections.emptySet();
        PrioritizedLenderPool lenderPool = new PrioritizedLenderPool(offers, doNothingPrioritizer);
        assertTrue(lenderPool.totalAvailableAmount() < 2000);
        lenderPool.borrow(2000);
    }

    @Test
    public void canBorrowWhenEnoughOffers() {
        PrioritizedLenderPool lenderPool = new PrioritizedLenderPool(offers, doNothingPrioritizer);
        lenderPool.borrow(200);
        assertThat(lenderPool.totalAvailableAmount(), is(equalTo(640 + 48 + 520 - 200)));
    }

    @Test
    public void borrowOperationUpdatesAvailableAmount() {
        PrioritizedLenderPool lenderPool = new PrioritizedLenderPool(offers, doNothingPrioritizer);
        lenderPool.borrow(640 + 48 + 20);
        assertThat(lenderPool.totalAvailableAmount(), is(equalTo(500)));
    }

    @Test
    public void whenUsingInterestRatePrioritizerTheLowerInterestRatesGetBorrowedFirst() {
        PrioritizedLenderPool lenderPool = new PrioritizedLenderPool(offers, new InterestRateOfferPrioritizer());
        lenderPool.borrow(48 + 20);
        assertThat(lenderPool.totalAvailableAmount(), is(equalTo(1140)));
        offers.forEach(offer -> {
            switch (offer.getLender()) {
                case "Jane":
                    assertThat(offer.getAmount().compareTo(BigDecimal.ZERO), is(equalTo(0)));
                    break;
                case "Fred":
                    assertThat(offer.getAmount().compareTo(BigDecimal.valueOf(500)), is(equalTo(0)));
                    break;
                case "Bob":
                    assertThat(offer.getAmount().compareTo(BigDecimal.valueOf(640)), is(equalTo(0)));
                    break;
                default:
                    break;
            }
        });
    }
}