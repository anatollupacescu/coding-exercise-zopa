package com.test;

import java.util.List;

public interface OfferPrioritizer {

    List<Offer> prioritize(List<Offer> offers);
}
