package com.test;

import java.math.BigDecimal;

public class Offer {

    private final String lender;
    private final String interest;
    private BigDecimal amount;

    public Offer(String lender, String interest, Integer amount) {
        this.lender = lender;
        this.interest = interest;
        this.amount = BigDecimal.valueOf(amount);
    }

    public String getLender() {
        return lender;
    }

    public String getInterest() {
        return interest;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
