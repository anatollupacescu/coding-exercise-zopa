package com.test;

import java.util.Comparator;
import java.util.List;

public class InterestRateOfferPrioritizer implements OfferPrioritizer {

    @Override
    public List<Offer> prioritize(List<Offer> offers) {
        offers.sort(Comparator.comparing(Offer::getInterest));
        return offers;
    }
}
