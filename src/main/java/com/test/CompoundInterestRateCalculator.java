package com.test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;

public class CompoundInterestRateCalculator implements RateCalculator {

    @Override
    public RateData calculate(Set<Loan> loans, int periodInMonths) {
        //this is the piece where I don't know what formula to apply...
        if (loans.isEmpty()) {
            throw new IllegalArgumentException("No loans to process");
        }
        BigDecimal totalBorrowed = new BigDecimal(0);
        BigDecimal totalIncludingInterest = new BigDecimal(0);
        for (Loan loan : loans) {
            totalBorrowed = totalBorrowed.add(loan.getAmount());
            totalIncludingInterest = totalIncludingInterest.add(calculateTotalWithInterest(loan, periodInMonths));
        }
        BigDecimal monthlyRepayment = totalIncludingInterest.divide(BigDecimal.valueOf(periodInMonths), 2, RoundingMode.HALF_UP);
        //I don't know how to calculate compound interest rate so I am just speculating here...
        BigDecimal interestRateInPercent = (totalIncludingInterest.subtract(totalBorrowed))
                                            .multiply(BigDecimal.valueOf(100))
                                            .divide(totalBorrowed, 2, RoundingMode.HALF_UP);
        return new RateData(totalBorrowed, periodInMonths, interestRateInPercent, monthlyRepayment, totalIncludingInterest.setScale(2, RoundingMode.HALF_UP));
    }

    private BigDecimal calculateTotalWithInterest(Loan loan, int periodInMonths) {
        //using this formuula amount*(1+interest_rate/12)^(total_months) - http://qrc.depaul.edu/StudyGuide2009/Notes/Savings%20Accounts/Compound%20Interest.htm
        BigDecimal annualRatePerMonth = loan.getRate().divide(BigDecimal.valueOf(12), RoundingMode.HALF_UP);
        BigDecimal compound = (BigDecimal.ONE.add(annualRatePerMonth)).pow(periodInMonths);
        return loan.getAmount().multiply(compound);
    }
}
