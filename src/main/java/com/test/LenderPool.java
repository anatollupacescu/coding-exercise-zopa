package com.test;

import java.util.Set;

public interface LenderPool {

    Set<Loan> borrow(int requestedAmount);
}
