package com.test;

import java.io.PrintStream;

public class RateDataConsolePresenter implements RateDataPresenter {

    private final RateData data;
    private final String template = "" +
            "Requested amount: £%s\n" +
            "Rate: %.1f%%\n" +
            "Monthly repayment: £%.2f\n" +
            "Total repayment: £%.2f\n";

    public RateDataConsolePresenter(RateData data) {
        this.data = data;
    }

    @Override
    public void present(PrintStream out) {
        out.print(String.format(template, data.getBorrowedAmount(), data.getInterestRate(), data.getMonthlyRepayment(), data.getTotalRepayment()));
        out.flush();
    }
}
