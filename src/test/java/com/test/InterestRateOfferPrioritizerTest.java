package com.test;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

public class InterestRateOfferPrioritizerTest {

    @Test
    public void prioritizeByInterestRate() throws Exception {
        final List<Offer> offers = new ArrayList<>();
        Offer offer1 = new Offer("Bob", "0.075", 640);
        Offer offer2 = new Offer("Jane", "0.069", 48);
        offers.add(offer1);
        offers.add(offer2);
        OfferPrioritizer prioritizer = new InterestRateOfferPrioritizer();
        List<Offer> processedOffers = prioritizer.prioritize(offers);
        assertThat(processedOffers.get(0), is(sameInstance(offer2)));
        assertThat(processedOffers.get(1), is(sameInstance(offer1)));
    }

    @Test
    public void prioritizeByAmount() {
        final List<Offer> offers = new ArrayList<>();
        Offer offer1 = new Offer("Bob", "0.075", 640);
        Offer offer2 = new Offer("Jane", "0.069", 48);
        offers.add(offer1);
        offers.add(offer2);
        OfferPrioritizer byAmount = (list) -> {
            list.sort(Comparator.comparing(Offer::getAmount));
            return list;
        };
        List<Offer> processedOffers = byAmount.prioritize(offers);
        assertThat(processedOffers.get(0), is(sameInstance(offer2)));
        assertThat(processedOffers.get(1), is(sameInstance(offer1)));
    }
}