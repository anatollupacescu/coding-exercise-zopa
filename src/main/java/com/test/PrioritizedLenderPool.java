package com.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PrioritizedLenderPool implements LenderPool {

    private final List<Offer> offers;

    public PrioritizedLenderPool(Set<Offer> offers, OfferPrioritizer prioritizer) {
        this.offers = prioritizer.prioritize(new ArrayList<>(offers));
    }

    public Set<Loan> borrow(int requestedAmount) {
        BigDecimal amount = BigDecimal.valueOf(requestedAmount);
        checkIfAmountIsAvailable(amount);
        return processRequest(amount);
    }

    private Set<Loan> processRequest(BigDecimal requestedAmount) {
        Set<Loan> loans = new HashSet<>();
        for (Offer offer : offers) {
            BigDecimal availableAmount = offer.getAmount();
            if (availableAmount.compareTo(requestedAmount) > 0) {
                offer.setAmount(availableAmount.subtract(requestedAmount));
                Loan loan = new Loan(requestedAmount, new BigDecimal(offer.getInterest()));
                loans.add(loan);
                return loans;
            } else {
                requestedAmount = requestedAmount.subtract(availableAmount);
                Loan loan = new Loan(availableAmount, new BigDecimal(offer.getInterest()));
                loans.add(loan);
                offer.setAmount(BigDecimal.ZERO);
            }
        }
        throw new IllegalStateException("Should have thrown NotEnoughOffersException");
    }

    private void checkIfAmountIsAvailable(BigDecimal requestedAmount) {
        int totalAvailable = totalAvailableAmount();
        if (requestedAmount.compareTo(BigDecimal.valueOf(totalAvailable)) > 0) {
            throw new NotEnoughOffersException();
        }
    }

    public int totalAvailableAmount() {
        return offers.stream().mapToInt(o -> o.getAmount().intValue()).sum();
    }
}
