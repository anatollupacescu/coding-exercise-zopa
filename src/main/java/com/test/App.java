package com.test;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class App {

    private final int periodInMonths;
    private final Set<Offer> offers;

    public static void main(String[] args) throws IOException {
        if (args.length < 2) throw new IllegalArgumentException("Please provide the file path and amount");
        String path = args[0];
        String amount = args[1];
        App app = new App(path, 36);
        app.processAmount(amount);
    }

    public App(String path, int months) {
        this.periodInMonths = months;
        try {
            this.offers = readOffersFromFile(path);
        } catch (IOException e) {
            throw new IllegalStateException("Could not read offers from file " + path);
        }
    }

    private Set<Offer> readOffersFromFile(String path) throws IOException {
        Set<Offer> offers = new HashSet<>();
        CSVReader reader = new CSVReader(new FileReader(path));
        String[] nextLine = reader.readNext(); //skipping header line
        while ((nextLine = reader.readNext()) != null) {
            String lender = nextLine[0];
            Offer offer = new Offer(lender, nextLine[1], Integer.valueOf(nextLine[2]));
            offers.add(offer);
        }
        return offers;
    }

    public void processAmount(String arg) {
        Integer amount = Integer.valueOf(arg);
        validateAmount(amount);
        LenderPool lenderPool = new PrioritizedLenderPool(offers, new InterestRateOfferPrioritizer());
        Set<Loan> loans = lenderPool.borrow(amount);
        RateCalculator calculator = new CompoundInterestRateCalculator();
        RateData data = calculator.calculate(loans, periodInMonths);
        RateDataPresenter presenter = new RateDataConsolePresenter(data);
        presenter.present(System.out);
    }

    private void validateAmount(Integer amount) {
        if (amount % 100 > 0 || amount < 1000 || amount > 15000) {
            throw new IllegalArgumentException("Amount should be divisible by 100, between 1000 and 15000");
        }
    }
}
