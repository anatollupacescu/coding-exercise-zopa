package com.test;

import java.util.Set;

public interface RateCalculator {

    RateData calculate(Set<Loan> loans, int periodInMonths);
}
