package com.test;

import java.math.BigDecimal;

public class RateData {

    private final BigDecimal borrowedAmount;
    private final int periodInMonths;
    private final BigDecimal interestRate;
    private final BigDecimal monthlyRepayment;
    private final BigDecimal totalRepayment;

    public RateData(BigDecimal borrowedAmount, int periodInMonths, BigDecimal interestRate, BigDecimal monthlyRepayment, BigDecimal totalRepayment) {
        this.borrowedAmount = borrowedAmount;
        this.periodInMonths = periodInMonths;
        this.interestRate = interestRate;
        this.monthlyRepayment = monthlyRepayment;
        this.totalRepayment = totalRepayment;
    }

    public BigDecimal getBorrowedAmount() {
        return borrowedAmount;
    }

    public int getPeriodInMonths() {
        return periodInMonths;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public BigDecimal getMonthlyRepayment() {
        return monthlyRepayment;
    }

    public BigDecimal getTotalRepayment() {
        return totalRepayment;
    }
}
