package com.test;

import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class CompoundInterestRateCalculatorTest {

    @Test
    public void returnRateData() throws Exception {
        RateCalculator rateCalculator = new CompoundInterestRateCalculator();
        Set<Loan> loans = new HashSet<>();
        int periodInMonths = 36;
        BigDecimal amount = BigDecimal.valueOf(1000);
        BigDecimal rate = new BigDecimal("0.07");
        loans.add(new Loan(amount, rate));
        RateData result = rateCalculator.calculate(loans, periodInMonths);
        assertThat(result, is(notNullValue()));
        assertThat(result.getBorrowedAmount(), is(equalTo(amount)));
    }

    @Ignore //I could not figure how to compute the data correctly
    @Test
    public void computeCorrectlyRateData() throws Exception {
        RateCalculator rateCalculator = new CompoundInterestRateCalculator();
        Set<Loan> loans = new HashSet<>();
        int periodInMonths = 36;
        BigDecimal amount = BigDecimal.valueOf(1000);
        BigDecimal rate = new BigDecimal("0.07");
        loans.add(new Loan(amount, rate));
        RateData result = rateCalculator.calculate(loans, periodInMonths);
        assertThat(result.getBorrowedAmount(), is(equalTo(amount)));
        assertThat(result.getInterestRate(), is(equalTo(rate.multiply(BigDecimal.valueOf(100)))));
        assertThat(result.getMonthlyRepayment(), is(equalTo(new BigDecimal("30.78"))));
        assertThat(result.getTotalRepayment(), is(equalTo(new BigDecimal("1108.10"))));
    }

}