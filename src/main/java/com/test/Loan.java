package com.test;

import java.math.BigDecimal;

public class Loan {

    private final BigDecimal amount;
    private final BigDecimal rate;

    public Loan(BigDecimal amount, BigDecimal rate) {
        this.amount = amount;
        this.rate = rate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getRate() {
        return rate;
    }
}
